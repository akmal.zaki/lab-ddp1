#Nama  = Akmal Zaki
#Kelas = DDP-1
#Npm   = 2106752205

# meminta user memasukkan nama file yang akan diinput
inp = input("Masukkan nama file input: ")

# meminta user memasukkan nama file output
out = input("Masukkan nama file output: ")

# kita akan memeriksa jika file yang di-read tidak ada maka
# errornya akan ditangkap dan mencetak peringatan
try:
    inFile = open(inp, "r")
    baris = inFile.readlines()
except FileNotFoundError:
    print("Nama file yang anda masukkan tidak ditemukan :(")
else:
    # untuk file output tidak perlu diperiksa karena kita menggunakan
    # metode 'w' untuk membuka filenya. Jika file ada maka akan menimpanya
    # jika tidak ada maka akan membuat file baru.
    outFile = open(out, "w")

    # menyiapkan variable yang dibutuhkan
    sum = 0
    realLine = ""
    realMessage = []

    # looping terhadap setiap baris
    for i in baris:

        # looping terhadap setiap karakter dalam baris
        for j in i:

            # jika karakter dalam baris adalah angka maka jumlahkan
            if (j.isnumeric()):
                sum += int(j)

            # jika karakter dalam baris adalah huruf atau spasi,
            # gabungkan ke dalam variable realLine
            elif(j.isalpha() or j == " "):
                realLine += j

        # jika nilai sum adalah genap, maka kita akan menyimpan baris tersebut
        # dalam array.
        if(sum % 2 == 0):
            realMessage.append(realLine+"\n")

        # mereset ulang nilai sum dan realine
        sum = 0
        realLine = ""

    # menghapus karakter new line pada baris terakhir
    realMessage[-1] = realMessage[-1].replace("\n", "")

    # mencetak realMessage ke dalam file output
    outFile.writelines(realMessage)

    # merubah metode outFile dari write menjadi read
    outFile = open(out, "r")

    # mencetak summary banyak baris dari input dan output
    print()
    print("Total baris dalam file input: " + str(len(baris)))
    print("Total baris yang disimpan: " + str(len(outFile.readlines())))

    # menutup file yang dibuka
    outFile.close()
    inFile.close()

