#Nama  = Akmal Zaki
#Kelas = DDP-1
#Npm   = 2106752205

# fungsi untuk menjumlahkan matriks
def penjumlahan() :
    ukuranA = input("Masukan ukuran matriks : ")
    x = int(ukuranA[0])
    y = int(ukuranA[4])
    matriksA = [[]]
    matriksB = [[]]

    # proses untuk input data ke matriks 1
    for i in range (x):
        baris = input("Baris {} matriks 1 : ".format(i + 1))
        angka = baris.split(" ")
        
        # memeriksa apakah ada eror
        if len(angka) > y:
            print("Terjadi kesalahan input. Silahkan ulang kembali.\n")
            return
        matriks = []
        for j in range (y) :
            matriks.append(int(angka[j]))
        matriksA.append(matriks)
    del matriksA[0]
    print("")

    # proses untuk input data ke matriks 2
    for i in range (x):
        baris = input("Baris {} matriks 2 : ".format(i + 1))
        angka = baris.split(" ")

        # memeriksa apakah ada eror
        if len(angka) > y:
            print("Terjadi kesalahan input. Silahkan ulang kembali.\n")
            return
        matriks = []
        for j in range (y) :
            
            matriks.append(int(angka[j]))
        matriksB.append(matriks)
    del matriksB[0]
    print("")

    # proses penjumlahan matriks
    print("hasil dari operasi : ")
    for x in range(0, len(matriksA)):
        for y in range(0, len(matriksA[0])):
            print (matriksA[x][y] + matriksB[x][y], end=' '),
        print()

# fungsi untuk pengurangan matriks
def pengurangan():
    ukuranA = input("Masukan ukuran matriks : ")
    x = int(ukuranA[0])
    y = int(ukuranA[4])
    matriksA = [[]]
    matriksB = [[]]

    # proses untuk input data ke matriks 1
    for i in range (x):
        baris = input("Baris {} matriks 1 : ".format(i + 1))
        angka = baris.split(" ")

        # memeriksa apakah ada eror
        if len(angka) > y:
            print("Terjadi kesalahan input. Silahkan ulang kembali.\n")
            return
        matriks = []
        for j in range (y) :
            
            matriks.append(int(angka[j]))
        matriksA.append(matriks)
    del matriksA[0]
    print("")

    # proses untuk input data ke matriks 2
    for i in range (x):
        baris = input("Baris {} matriks 2 : ".format(i + 1))
        angka = baris.split(" ")

        # memeriksa apakah ada eror
        if len(angka) > y:
            print("Terjadi kesalahan input. Silahkan ulang kembali.\n")
            return
        matriks = []
        for j in range (y) :
            
            matriks.append(int(angka[j]))
        matriksB.append(matriks)
    del matriksB[0]
    print("")

    # proses pengurangan matriks
    print("hasil dari operasi : ")
    for x in range(0, len(matriksA)):
        for y in range(0, len(matriksA[0])):
            print (matriksA[x][y] - matriksB[x][y], end=' '),
        print()

# fungsi untuk transpose matriks
def transpose():
    ukuranA = input("Masukan ukuran matriks : ")
    x = int(ukuranA[0])
    y = int(ukuranA[4])
    matriksA = [[]]

    # proses untuk input data ke matriks 
    for i in range (x):
        baris = input("Baris {} matriks : ".format(i + 1))
        angka = baris.split(" ")

        # memeriksa apakah ada eror
        if len(angka) > y:
            print("Terjadi kesalahan input. Silahkan ulang kembali.\n")
            return
        matriks = []
        for j in range (y) :
            matriks.append(int(angka[j]))
        matriksA.append(matriks)
    del matriksA[0]
    print("")

    # proses transpose matriks
    print("hasil dari operasi : ")
    for i in range(y):
        for j in range(x):
            print(matriksA[j][i], end = ' ')
        print()

# fungsi untuk determinan matriks
def determinan() :
    x = 2
    y = 2
    matriksA = [[]]

    # proses untuk input data ke matriks 
    for i in range (x):
        baris = input("Baris {} matriks : ".format(i + 1))
        angka = baris.split(" ")

        # memeriksa apakah ada error
        if len(angka) > y:
            print("Terjadi kesalahan input. Silahkan ulang kembali.\n")
            return
        matriks = []
        for j in range (y) :
            matriks.append(int(angka[j]))
        matriksA.append(matriks)
    del matriksA[0]
    print("")

    # proses perhitungan
    print("hasil dari operasi : ")
    print((matriksA[0][0] * matriksA[1][1]) - (matriksA[0][1] * matriksA[1][0]))
# while menu print seperti di soal
while (True) :
    print("Selamat datang di Matrix Calculator. Berikut adalah operasi operasi yang dapat dilakukan :")
    print("1. Penjumlahan")
    print("2. Pengurangan")
    print("3. Transpose")
    print("4. Determinan")
    print("5. Keluar")
    pilih = int(input("Silahkan pilih operasi : "))

    if pilih == 1 :
        penjumlahan()
    elif pilih == 2 :
        pengurangan()
    elif pilih == 3 :
        transpose()
    elif pilih == 4 :
        determinan()
    elif pilih == 5 :
        print("Sampai Jumpa!")
        break
    else :
        print("Masukan pilihan sesuai menu")