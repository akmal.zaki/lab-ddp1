#Nama  = Akmal Zaki
#Kelas = DDP-1
#Npm   = 2106752205

# import random untuk .randint
import random

# meminta user memasukkan pesan yang ingin dikirim
pesan = input("Masukkan pesan yang akan dikirim: ")

# mengubah seluruh teks menjadi lower case
pesan = pesan.lower()

# meminta user memasukkan n untuk caeser encryption
n = input("Pilih nilai n: ")

# mengonversi string menjadi int
n = int(n)

enc_pesan = ""

# proses caesar chiper
for i in range(len(pesan)):
    if pesan[i].isnumeric():
        # pergeseran setiap karakter berdasarkan n
        new_char = (ord(pesan[i]) + n - 48) % 10 + 48
        new_char = (new_char + n - 48) % 10 + 48
        enc_pesan += chr(new_char)
    elif pesan[i] == ' ':
        # jika spasi terdeteksi, jangan enkripsi
        enc_pesan += pesan[i]
    else:
        # pergeseran setiap karakter number berdasarkan n
        new_char = (ord(pesan[i]) + n - 97) % 26 + 97
        enc_pesan += chr(new_char)

paragraf_enc = ""
i = 0

# membuka file paragraf.txt
with open("paragraf.txt") as f:
    mark = 0
    # menggunakan f.read() untuk membaca
    paragraf = f.read() 
    # menggunakan random.randint untuk merandom posisi kunci
    # menentukan posisi kunci n
    kunci_n_pos = random.randint(100, len(paragraf)-100) 

    # menggenerate kunci n
    kunci_n = 20220310 - n
    final_kunci_n = '*' + str(kunci_n) + '*'

    # menyamarkan pesan teks yang ingin dikirim ke dalam paragraf
    while i < len(paragraf):
        if mark < len(enc_pesan):
            if enc_pesan[mark].isnumeric():
                paragraf_enc += enc_pesan[mark]
                mark += 1
                i -= 1
            elif enc_pesan[mark] == " ":
                paragraf_enc += '$'
                mark += 1
                i -= 1
            else:
                if enc_pesan[mark] == paragraf[i]:
                    paragraf_enc += paragraf[i].upper()
                    mark += 1
                else:
                    paragraf_enc += paragraf[i]
        else:
            paragraf_enc += paragraf[i]
        i += 1

# menggabungkan kunci ke dalam paragraf pada posisi random
paragraf_enc = paragraf_enc[:kunci_n_pos] + \
    final_kunci_n + paragraf_enc[kunci_n_pos:]

# print paragraf yang sudah di enkripsi
print(paragraf_enc)

