#Namac = Akmal Zaki
#Kelas = DDP-1
#Npm   = 2106752205

harga_pulpen = 10000
harga_pensil = 5000
harga_cortape = 15000

print("Toko ini menjual: ")
print("1. Pulpen"+ "  ("+ str(harga_pulpen)+ "/pcs"+")")
print("2. Pensil"+ "  ("+ str(harga_pensil)+ "/pcs"+")")
print("3. Correction tape"+ "  ("+ str(harga_cortape)+ "/pcs"")")
print()
print("Masukkan jumlah yang ingin anda beli:")
total_pulpen = int(input("Pulpen: "))
total_pensil = int(input("Pensil: "))
total_cortape = int(input("Correction tape: "))
total_harga = (total_pulpen * harga_pulpen) + \
    (total_pensil * harga_pensil) + (total_cortape * harga_cortape)
print()
print("Ringkasan pembelian: ")
print(str(total_pulpen)+ " pulpen x " + str(harga_pulpen))
print(str(total_pensil)+ " pensil x " + str(harga_pensil))
print(f"{total_cortape} correction tape x { harga_cortape}")
print(f"Total harga: {total_harga}")
